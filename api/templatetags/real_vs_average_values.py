from django import template

register = template.Library()


@register.filter(name='difference')
def difference(value, average):
    return float(value) - float(average)
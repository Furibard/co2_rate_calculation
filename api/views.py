import requests
from datetime import datetime
from dateutil.parser import parse
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.utils.timezone import make_aware
from django.db.models import Avg
from api.models import ConsumptionCO2, ConsumptionCO2AveragePerHour
from api.utils import checkIsWorkingDay, calculationAverageWorkingDay


def fetchData(request):
    kwargs = {}

    if ConsumptionCO2.objects.exists():
        calculationAverageWorkingDay()

        return render(request, 'table.html', params)

    # Paramètres pour les années 2017 et 2018
    payload = {"start": "1483225200", "end": "1546297199"}

    responses = requests.get("http://api-recrutement.ecoco2.com/v1/data/", params=payload).json()

    co2_rate_list = []

    """
        JE LIMITE VOLONTAIREMENT L'ENREGISTREMENT EN DB AVEC LES 50 DERNIERS ELEMENTS REÇUS
        AU LIEU DE LA TOTALITÉ, AFIN D'EVITER UN LONG DÉLAI D'ATTENTE
    """
    for response in responses[-50:]:
        parse_datetime = make_aware(parse(response["datetime"]))
        working_day = checkIsWorkingDay(parse_datetime)
        co2_rate_list.append(parse_datetime)

        if len(co2_rate_list) == 1:
            start_hour = parse_datetime.hour
            start_datetime = parse_datetime
            start_co2_rate = response["co2_rate"]

        elif len(co2_rate_list) == 2 and (parse_datetime.date() == co2_rate_list[0].date() \
            and parse_datetime.hour == co2_rate_list[0].hour):

            stop_hour = parse_datetime.hour
            stop_datetime = parse_datetime
            stop_co2_rate = response["co2_rate"]

            average_co2_per_hour = (start_co2_rate + stop_co2_rate) / 2

            average = ConsumptionCO2AveragePerHour.objects.create(co2_rate_average=average_co2_per_hour)

            ConsumptionCO2.objects.create(
                datetime=start_datetime,
                co2_rate=start_co2_rate,
                average_per_hour=average,
                is_working_day=working_day
            )

            ConsumptionCO2.objects.create(
                datetime=stop_datetime,
                co2_rate=stop_co2_rate,
                average_per_hour=average,
                is_working_day=working_day
            )

            co2_rate_list = []

        else:
            average = ConsumptionCO2AveragePerHour.objects.create(co2_rate_average=start_co2_rate)

            ConsumptionCO2.objects.create(
                datetime=start_datetime,
                co2_rate=start_co2_rate,
                average_per_hour=average,
                is_working_day=working_day
            )

            co2_rate_list = []
            co2_rate_list.append(parse_datetime)
            start_hour = parse_datetime.hour
            start_datetime = parse_datetime
            start_co2_rate = response["co2_rate"]

    params = calculationAverageWorkingDay()

    return render(request, 'table.html', params)


def graphic(request):
    responses = ConsumptionCO2.objects.exists()

    if not responses:
        return redirect('table')

    return render(request, 'graphic.html', {"responses": responses})


def displayGraphic(request):
    labels = []
    data = []
    raw_co2_values = []
    average_co2_values = []
    hours_values = []

    consumption_co2_set = ConsumptionCO2.objects.order_by('-id')[:20][::-1]

    for value in consumption_co2_set:
        raw_co2_values.append(value.co2_rate)
        average_co2_values.append(value.average_per_hour.co2_rate_average)
        hours_values.append(value.datetime.strftime("%H:%M"))

    data.append({"raw_co2_values": raw_co2_values})
    data.append({"average_co2_values": average_co2_values})
    data.append({"day": consumption_co2_set[0].datetime.strftime("%d-%m-%Y")})
    labels.append(hours_values)

    return JsonResponse(data={'labels': labels, 'data': data})

def table(request):
    params = calculationAverageWorkingDay()

    return render(request, 'table.html', params)


def deleteData(request):
    ConsumptionCO2.objects.all().delete()

    return HttpResponse()
import workalendar.europe
from django.db.models import Avg
from api.models import ConsumptionCO2


def checkIsWorkingDay(day):
    cal = workalendar.europe.France()
    working_day = False

    # Vérifie que le jour n'est pas un samedi ou un dimanche
    if not day.weekday() in [5, 6]:
        # Vérifie que le jour n'est pas un jour férié
        if cal.is_working_day(day):
            working_day = True

    return working_day


def calculationAverageWorkingDay():
    # Calcul les moyennes de CO2 pour les jours ouvrés et les weekends
    params = {
        "average_working_day": ConsumptionCO2.objects.filter(is_working_day=True).aggregate(Avg('co2_rate')).get("co2_rate__avg"),
        "average_weekend": ConsumptionCO2.objects.filter(is_working_day=False).aggregate(Avg('co2_rate')).get("co2_rate__avg"),
        "responses": ConsumptionCO2.objects.order_by('-id')[:20]
    }

    return params